//
//  ViewController.h
//  2048
//
//  Created by Vaster on 2/4/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (nonatomic, strong) IBOutlet UILabel *square1;
@property (nonatomic, strong) IBOutlet UILabel *square2;
@property (nonatomic, strong) IBOutlet UILabel *square3;
@property (nonatomic, strong) IBOutlet UILabel *square4;
@property (nonatomic, strong) IBOutlet UILabel *square5;
@property (nonatomic, strong) IBOutlet UILabel *square6;
@property (nonatomic, strong) IBOutlet UILabel *square7;
@property (nonatomic, strong) IBOutlet UILabel *square8;
@property (nonatomic, strong) IBOutlet UILabel *square9;
@property (nonatomic, strong) IBOutlet UILabel *square10;
@property (nonatomic, strong) IBOutlet UILabel *square11;
@property (nonatomic, strong) IBOutlet UILabel *square12;
@property (nonatomic, strong) IBOutlet UILabel *square13;
@property (nonatomic, strong) IBOutlet UILabel *square14;
@property (nonatomic, strong) IBOutlet UILabel *square15;
@property (nonatomic, strong) IBOutlet UILabel *square16;
@property (nonatomic, strong) IBOutlet UILabel *score;




@property (nonatomic, strong) IBOutlet UIButton *upButton;
@property (nonatomic, strong) IBOutlet UIButton *downButton;
@property (nonatomic, strong) IBOutlet UIButton *rightButton;
@property (nonatomic, strong) IBOutlet UIButton *leftButton;
@property (nonatomic, strong) IBOutlet UIButton *resetButton;
    
@property (strong, nonatomic) IBOutlet UIImageView *imageView;


-(IBAction)upAction:(id)up;
-(IBAction)downAction:(id)down;
-(IBAction)rightAction:(id)right;
-(IBAction)leftAction:(id)left;
-(IBAction)reser:(id)restart;
-(void) updateLabels;
+(void) moveUp;
+(void) moveRight;
+(void) moveLeft;
+(void) moveDown;
+(void) insertRandomBox;
+(int) calcScore;



@end

