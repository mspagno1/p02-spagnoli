//
//  AppDelegate.h
//  2048
//
//  Created by Vaster on 2/4/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

