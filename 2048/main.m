//
//  main.m
//  2048
//
//  Created by Vaster on 2/4/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
/*

Boolean canNumbersCombine(int num1, int num2){
    if(num1 == num2){
        return true;
    }
    else{
        return false;
    }
}

int randomBox(int numOfBox,int boxes[numOfBox] ){
    bool emptyBox = false;
    int boxNum = 0;
    while(emptyBox == false){
        boxNum = rand() % 16;
        if(boxes[boxNum] == 0){
            emptyBox = true;
        }
    }
    return boxNum;
}

void moveUp(int row, int col, int grid[row][col]){
    for(int i = 1; i< 4; i++){
        for(int j = 0;j < 4; j++){
            //If grid spot is empty we can move up
            if(grid[i-1][j] == 0){
                grid[i-1][j] = grid[i][j];
                grid[i][j] = 0;
            }
            //If there numbers can combine combine them
            else if(canNumbersCombine(grid[i-1][j],grid[i][j]) == true){
                grid[i-1][j] += grid[i][j];
                grid[i][j] = 0;
            }
        }
    }
}

void moveDown(int row, int col, int grid[row][col]){
    for(int i = 3; i > 0; i--){
        for(int j = 0;j < 4; j++){
            //If grid spot is empty we can move up
            if(grid[i+1][j] == 0){
                grid[i+1][j] = grid[i][j];
                grid[i][j] = 0;
            }
            //If there numbers can combine combine them
            else if(canNumbersCombine(grid[i+1][j],grid[i][j]) == true){
                grid[i+1][j] += grid[i][j];
                grid[i][j] = 0;
            }
        }
    }
}


void moveLeft(int row, int col, int grid[row][col]){
    for(int i = 0; i < 4; i++){
        for(int j = 1;j < 4; j++){
            //If grid spot is empty we can move up
            if(grid[i][j-1] == 0){
                grid[i][j-1] = grid[i][j];
                grid[i][j] = 0;
            }
            //If there numbers can combine combine them
            else if(canNumbersCombine(grid[i][j-1],grid[i][j]) == true){
                grid[i][j-1] += grid[i][j];
                grid[i][j] = 0;
            }
        }
    }
}

void moveRight(int row, int col, int grid[row][col]){
    for(int i = 0; i < 4; i++){
        for(int j = 4;j > 0; j--){
            //If grid spot is empty we can move up
            if(grid[i][j+1] == 0){
                grid[i][j+1] = grid[i][j];
                grid[i][j] = 0;
            }
            //If there numbers can combine combine them
            else if(canNumbersCombine(grid[i][j+1],grid[i][j]) == true){
                grid[i][j+1] += grid[i][j];
                grid[i][j] = 0;
            }
        }
    }
}

*/


int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
